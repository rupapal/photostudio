import { Component, Prop, h } from '@stencil/core';
import { format } from '../../utils/utils';

@Component({
  tag: 'my-footer',
  styleUrl: 'my-footer.css',
  shadow: true
})
export class MyFooter {
  /**
   * The first name
   */
  @Prop() first: string;

  /**
   * The middle name
   */
  @Prop() middle: string;

  /**
   * The last name
   */
  @Prop() last: string;

  private getText(): string {
    return format(this.first, this.middle, this.last);
  }

  render() { 
    return (
		<footer class="footer">
			<div class="container">
				<ul>
					<li><a href="https://www.facebook.com/" target="_blank"><img src="./assets/facebook-icon.png" alt="Facebook" /></a></li>
					<li><a href="https://twitter.com" target="_blank"><img src="./assets/twitter-icon.png" alt="Twitter" /></a></li>
					<li><a href="https://instagram.com" target="_blank"><img src="./assets/insta-icon.png" alt="Instagram" /></a></li>
					<li><a href="https://in.pinterest.com/" target="_blank"><img src="./assets/pintrest-icon.png" alt="Pinterest" /></a></li>
				</ul>
				<nav>
					<li><a href="#">About Us</a></li>
					<li><a href="#">Sponsor Contest</a></li>
					<li><a href="#">Privacy</a></li>
					<li><a href="#">Terms</a></li>
					<li><a href="#">Support</a></li> 
				</nav> 
				<div class="copyright">Copyright &copy;2019 Photostudio. All right reserved</div> 
			</div>
		</footer>
	)}
}

import { Component, Prop, h } from '@stencil/core';
import { format } from '../../utils/utils';

@Component({
  tag: 'my-header',
  styleUrl: 'my-header.css',
  shadow: true
})
export class MyHeader {
  /**
   * The first name
   */
  @Prop() first: string;

  /**
   * The middle name
   */
  @Prop() middle: string;

  /**
   * The last name
   */
  @Prop() last: string;

  private getText(): string {
    return format(this.first, this.middle, this.last);
  }

  render() { 
    return (
		<header class="header"> 
			<div class="container">
				<a href="#" class="app-logo">  
					<img src="./assets/photo-studio-logo.png" alt="App Name" /> 
				</a>
				<button type="button">LOGIN</button>
				<nav>
					<li><a href="#">PHOTO SHOOTS</a></li>
					<li><a href="#">GALLERIES</a></li>
					<li><a href="#">WIN</a></li>
					<li><a href="#">ABOUT</a></li>
					<li><a href="#">MAGAZINE</a></li>
				</nav> 
			</div>
		</header>
	)}
}

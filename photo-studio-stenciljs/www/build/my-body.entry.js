import { e as registerInstance, f as h } from './photo-studio-3c844eda.js';
import { a as format } from './chunk-b1e8e431.js';

class MyBody {
    constructor(hostRef) {
        registerInstance(this, hostRef);
    }
    getText() {
        return format(this.first, this.middle, this.last);
    }
    render() {
        return (h("section", null));
    }
    static get style() { return "* {\n    margin: 0;\n    padding: 0;\n    -webkit-box-sizing: border-box;\n    box-sizing: border-box;\n}\nsection {\n    min-height: calc(100vh - 289px);\n    display: inline-block;\n    background: #000 !important;\n    width: 100%;\n    float: left;\n}"; }
}

export { MyBody as my_body };

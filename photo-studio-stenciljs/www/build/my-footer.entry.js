import { e as registerInstance, f as h } from './photo-studio-3c844eda.js';
import { a as format } from './chunk-b1e8e431.js';

class MyFooter {
    constructor(hostRef) {
        registerInstance(this, hostRef);
    }
    getText() {
        return format(this.first, this.middle, this.last);
    }
    render() {
        return (h("footer", { class: "footer" }, h("div", { class: "container" }, h("ul", null, h("li", null, h("a", { href: "https://www.facebook.com/", target: "_blank" }, h("img", { src: "./assets/facebook-icon.png", alt: "Facebook" }))), h("li", null, h("a", { href: "https://twitter.com", target: "_blank" }, h("img", { src: "./assets/twitter-icon.png", alt: "Twitter" }))), h("li", null, h("a", { href: "https://instagram.com", target: "_blank" }, h("img", { src: "./assets/insta-icon.png", alt: "Instagram" }))), h("li", null, h("a", { href: "https://in.pinterest.com/", target: "_blank" }, h("img", { src: "./assets/pintrest-icon.png", alt: "Pinterest" })))), h("nav", null, h("li", null, h("a", { href: "#" }, "About Us")), h("li", null, h("a", { href: "#" }, "Sponsor Contest")), h("li", null, h("a", { href: "#" }, "Privacy")), h("li", null, h("a", { href: "#" }, "Terms")), h("li", null, h("a", { href: "#" }, "Support"))), h("div", { class: "copyright" }, "Copyright \u00A92019 Photostudio. All right reserved"))));
    }
    static get style() { return "* {\n    margin: 0;\n    padding: 0;\n    -webkit-box-sizing: border-box;\n    box-sizing: border-box;\n}\n.footer {\n    background: #1d1d1b;\n    float: left;\n    width: 100%;\n	padding: 60px 0 55px;\n}\n.container {\n    max-width: 1170px;\n    margin: 0 auto;\n    padding: 0 15px;\n}\n.footer li {\n    list-style: none;\n    display: inline-block;\n    margin: 0 6px;\n}\n.footer ul li{\n    max-width: 30px;\n}\n.footer ul {\n    text-align: center;\n    margin-bottom: 15px;\n}\n.footer nav {\n    text-align: center;\n}\n.footer li a {\n    font-size: 14px;\n}\n.footer nav li a {\n    text-decoration: unset !important;\n    font-family: sans-serif;\n    color: #fff;\n    margin-top: 5px !important;\n    display: block;\n}\n.copyright {\n    text-align: center;\n    color: #808080;\n    font-family: sans-serif;\n    margin-top: 10px;\n	font-size: 14px;\n}\n.footer li a img {\n    width: 100%;\n}"; }
}

export { MyFooter as my_footer };

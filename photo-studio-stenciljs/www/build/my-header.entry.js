import { e as registerInstance, f as h } from './photo-studio-3c844eda.js';
import { a as format } from './chunk-b1e8e431.js';

class MyHeader {
    constructor(hostRef) {
        registerInstance(this, hostRef);
    }
    getText() {
        return format(this.first, this.middle, this.last);
    }
    render() {
        return (h("header", { class: "header" }, h("div", { class: "container" }, h("a", { href: "#", class: "app-logo" }, h("img", { src: "./assets/photo-studio-logo.png", alt: "App Name" })), h("button", { type: "button" }, "LOGIN"), h("nav", null, h("li", null, h("a", { href: "#" }, "PHOTO SHOOTS")), h("li", null, h("a", { href: "#" }, "GALLERIES")), h("li", null, h("a", { href: "#" }, "WIN")), h("li", null, h("a", { href: "#" }, "ABOUT")), h("li", null, h("a", { href: "#" }, "MAGAZINE"))))));
    }
    static get style() { return "* {\n    margin: 0;\n    padding: 0;\n    -webkit-box-sizing: border-box;\n    box-sizing: border-box;\n}\n.container {\n    max-width: 1170px;\n    margin: 0 auto;\n	padding: 0 15px;\n}\n.header {\n    padding: 15px 0 12px;\n}\n.header nav li {\n    list-style: none;\n    display: inline-block;\n    padding: 10px;\n    margin: 0 15px;\n}\n.header nav {\n    display: inline-block;\n    float: right;\n    margin-top: 5px;\n}\n.header button {\n    background: #9b204c;\n    border: none;\n    color: #fff;\n    padding: 10px 20px;\n    float: right;\n    margin-top: 4px;\n    border-radius: 3px;\n    margin-left: 15px;\n    outline: none;\n    cursor: pointer;\n}\n.app-logo {\n    display: inline-block;\n}\n.header nav li a {\n    text-decoration: unset !important;\n    font-family: sans-serif;\n    color: #000;\n    font-weight: bold;\n}"; }
}

export { MyHeader as my_header };

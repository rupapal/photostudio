import { a as patchBrowser, b as globals, c as bootstrapLazy } from './photo-studio-3c844eda.js';

patchBrowser().then(resourcesUrl => {
  globals();
  return bootstrapLazy([["my-body",[[1,"my-body",{"first":[1],"middle":[1],"last":[1]}]]],["my-footer",[[1,"my-footer",{"first":[1],"middle":[1],"last":[1]}]]],["my-header",[[1,"my-header",{"first":[1],"middle":[1],"last":[1]}]]]], { resourcesUrl });
});
